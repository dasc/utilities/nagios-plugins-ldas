#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName DISKCACHE

set options $nagios::options
lappend options \
    [list d.arg /etc/diskcache/diskcache.rsc "Path to diskcache configuration file."] \
    [list l.arg /var/log/diskcache/diskcache.log.txt "Path to diskcache log file."] \
    [list n.arg -1 "Number of mounts to expect.  Overrides contents of diskcache.rsc"]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

if {![info exists option(l)]} {
    nagios::exit_critical "No log directory given with -l"
}

if {[info exists option(n)] && $option(n) > 0} {
    set mountCount $option(n)
} elseif {[info exists option(d)]} {
    set fileId [open $option(d) r]
    set mountCount 0
    set markerFound 0
    while {![eof $fileId]} {
	gets $fileId line
	if {![eof $fileId]} {
	    if {[string equal {[MOUNT_POINTS]} $line ]} {
		set markerFound 1
	    } elseif {[string length $line] > 0 && [string index $line 0] != "#" && $markerFound} {
	        incr mountCount
	    }
	}
    }
    catch {close $fileId}
} else {
    nagios::exit_critical "One of -n or -d must be specified"
}

#A matching line looks like this:
#***PURPLE*** 1158946786 [[SCAN_MOUNTPT]] {ScanMountPointList} 43 mount points, 41140 directories, 57115112 files, scanned in 16709 ms (00:00:16.709).

set loggedMounts 0
set fileId [open $option(l) r]
while {![eof $fileId]} {
    gets $fileId line
    if {![eof $fileId]} {
	if {[regexp {{ScanMountPointList} (\d+) mount points} $line null matchStr]} {
	    set loggedMounts $matchStr
	}
    }
}
catch {close $fileId}

if {$loggedMounts == 0} {
    nagios::exit_critical "diskcache has not reported finding any mounts in the log file."
} elseif {$loggedMounts != $mountCount} {
    nagios::exit_critical "Number of mounts don't match: $mountCount != $loggedMounts"
} else {
    nagios::exit_ok "$loggedMounts mounts scanned"
}

nagios::exit_ok "Certificate expires on $expireDate"
