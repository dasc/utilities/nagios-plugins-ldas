#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName NETROUTE

set options $nagios::options
lappend options \
    [list e "Indicates that there should be no default gateway"]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

set allowedGateways $argv

set iprouteCmd [list /usr/sbin/ip route show default 0.0.0.0/0] 

set defaultGateway [list]

set chanId [open |$iprouteCmd r]
while {![eof $chanId]} {
    gets $chanId line
    if ![eof $chanId] {
        lappend defaultGateway [lindex $line 2]
    }
}
catch {close $chanId}

if {[llength $defaultGateway] == 0} {
    if {$option(e)} {
	nagios::exit_ok "No gateway found for default route"
    } else {
	nagios::exit_critical "No gateway found for default route"
    }
} elseif {$option(e)} {
    nagios::exit_critical "gateway found when there should be none: $defaultGateway"
}

if {[llength $defaultGateway] > 1} {
    nagios::exit_critical "Multiple gateways found for default route: $defaultGateway"
}

if {[llength $allowedGateways] == 0} {
    nagios::exit_ok "Default gateway found:  $defaultGateway"
} elseif {[lsearch $allowedGateways $defaultGateway] >= 0} {
    nagios::exit_ok "Default gateway ok:  $defaultGateway"
}

nagios::exit_critical "Default gateway is not allowed:  $defaultGateway (should be one of $allowedGateways)"
