#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName MTU

set options $nagios::options
lappend options \
    [list d.arg {} "interface device name"] \
    [list w.arg "" "Warning threshhold for mtu size"] \
    [list c.arg "" "Critical threshhold for mtu size"]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

set selLimit 10

set ipCmd [list /usr/sbin/ip link show]
if {$option(d) != {}} {
    lappend ipCmd $option(d)
} else {
    nagios::exit_unknown "Missing -d flag for interface name"
}

set exitStatus ok
set exitMessage ""
set exitSeparator ""
set mtu {}

set chanId [open |$ipCmd r]
while {![eof $chanId]} {
    gets $chanId line
    if {[regexp {(mtu|MTU) (\d+)} $line null match1 match2]} {
        set mtu $match2
    }
}
if {[catch {close $chanId} errmsg]} {
    nagios::exit_unknown $errmsg
}

if {$mtu == ""} {
    nagios::exit_critical "MTU not found for interface $option(d)"
}

if {$option(c) != "" && ! [nagios::compareToRange $mtu $option(c)]} {
    nagios::exit_critical "MTU $mtu not within range: $option(c)"
}

if {$option(w) != "" && ! [nagios::compareToRange $mtu $option(w)]} {
    nagios::exit_warning "MTU $mtu not within range: $option(w)"
}

nagios::exit_ok "MTU found: $mtu"
