#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName X509CERT

set options $nagios::options
lappend options \
    [list cert.arg /etc/grid-security/hostcert.pem "Path to x509 certificate."] \
    [list w.arg 168 "Warning threshhold, in hours"] \
    [list c.arg 1 "Critical threshhold, in hours"] \
    [list certType.arg x509 "Certificate type."]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

#Parse for cert type, defaults to x509, switch to crl on -crl argument
if {$option(certType) == "crl"} {
    set checkend -nextupdate
    set enddate -nextupdate
} elseif {$option(certType) == "x509"} { 
    set checkend -checkend
    set enddate -enddate 
} else {
    puts "Unknown certificate type, please use x509 or crl for type."
    exit 1
}

if {![info exists option(cert)]} {
    set option(cert) /etc/grid-security/hostcert.pem
}

set opensslCmd [list /usr/bin/openssl $option(certType) -noout -in $option(cert)]

set criticalCheckCmd [linsert $opensslCmd end $enddate]
set warnCheckCmd [linsert $opensslCmd end $enddate]
set getExpireCmd [linsert $opensslCmd end $enddate]

# Get current date
set systemTime [clock seconds]

# Sanity checks that the host certificate can be read
if {![file exists $option(cert)]} {
    nagios::exit_critical "Host certificate not found: $option(cert)"
}

if {![file readable $option(cert)]} {
    if {$option(v)} {
	puts "User $env::USER could not read certificate $option(cert)"
    }
    nagios::exit_critical "Certificate could not be read: $option(cert)"
}

if {[catch {set expireDate [eval exec $getExpireCmd]} msg]} {
    if {$option(v)} {
	puts "Error getting certificate expiration date: $::errorInfo\n$msg"
    }
    nagios::exit_unknown "Could not determine certificate expiration date"
}

#Remove unwanted portion of string
if {$option(certType) == "x509"} {
    regsub {^notAfter=} $expireDate {} expireDate
} else {
    regsub {^nextUpdate=} $expireDate {} expireDate
}
#Convert to UNIX time.
set expDate [clock scan $expireDate]

#Compare today's date with expire date for critical then warning states, in UNIX time.
set difference [expr $expDate - $systemTime]
set warning [expr $option(w) * 3600]
set critical [expr $option(c) * 3600]

if {$difference < $critical} {
    if {$option(v)} {
	puts "Critical check failed: $::errorInfo\n$msg"
    }
    nagios::exit_critical "Certificate expires on $expireDate"
}
if {$difference < $warning} {
    if {$option(v)} {
	puts "Warning check failed: $::errorInfo\n$msg"
    }
    nagios::exit_warning "Certificate expires on $expireDate"
}

nagios::exit_ok "Certificate expires on $expireDate"
