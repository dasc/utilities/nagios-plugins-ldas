#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName IPMI

set options $nagios::options
lappend options \
    [list U.arg {} "ipmi remote username"] \
    [list H.arg {} "ipmi remote host"] \
    [list P.arg {} "ipmi remote password"] \
    [list ignore.arg {} "List of sensors to ignore"] \
    [list param.arg temp "The name of a SDR parameter"] \
    [list e "Generate warning/error based on the number of event log entries"] \
    [list w.arg 0 "Warning threshhold, in C for temperatures or rpm for fan speeds"] \
    [list c.arg 0 "Critical threshhold, in C for temperatures or rpm for fan speeds"]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

set selLimit 10

set ipmiCmdPrefix [list /usr/bin/ipmitool]
if {$option(U) != {}} {
    append ipmiCmdPrefix " -U $option(U)"
}
if {$option(H) != {}} {
    append ipmiCmdPrefix " -I lanplus -H $option(H)"
}
if {$option(P) != {}} {
    append ipmiCmdPrefix " -P $option(P)"
}

# Sample output:
#CPU1 Temp        | 0.000      | unspecified | ok    | na        | na        | na        | na        | na        | na        
#CPU2 Temp        | 0.000      | unspecified | ok    | na        | na        | na        | na        | na        | na        
#System Temp      | 44.000     | degrees C  | ok    | 0.000     | 0.000     | 0.000     | 81.000    | 82.000    | 83.000    
#CPU1 Vcore       | 1.016      | Volts      | ok    | 0.808     | 0.816     | 0.824     | 1.384     | 1.392     | 1.400     
#CPU2 Vcore       | 1.072      | Volts      | ok    | 0.808     | 0.816     | 0.824     | 1.384     | 1.392     | 1.400     
#+5V              | 5.120      | Volts      | ok    | 4.280     | 4.320     | 4.360     | 5.240     | 5.280     | 5.320     
#+12V             | 11.904     | Volts      | ok    | 10.464    | 10.560    | 10.656    | 13.344    | 13.440    | 13.536    
#CPU1DIMM         | 1.544      | Volts      | ok    | 1.320     | 1.328     | 1.336     | 1.656     | 1.664     | 1.672     
#CPU2DIMM         | 1.536      | Volts      | ok    | 1.320     | 1.328     | 1.336     | 1.656     | 1.664     | 1.672     
#+1.5V            | 1.504      | Volts      | ok    | 1.320     | 1.328     | 1.336     | 1.656     | 1.664     | 1.672     
#+3.3V            | 3.240      | Volts      | ok    | 2.880     | 2.904     | 2.928     | 3.648     | 3.672     | 3.696     
#+3.3VSB          | 3.360      | Volts      | ok    | 2.880     | 2.904     | 2.928     | 3.648     | 3.672     | 3.696     
#VBAT             | 3.360      | Volts      | ok    | 2.880     | 2.904     | 2.928     | 3.648     | 3.672     | 3.696     
#Fan1             | 9384.000   | RPM        | ok    | 340.000   | 408.000   | 476.000   | 17204.000 | 17272.000 | 17340.000 
#Fan2             | 9384.000   | RPM        | ok    | 340.000   | 408.000   | 476.000   | 17204.000 | 17272.000 | 17340.000 
#Fan3             | na         | RPM        | na    | 340.000   | 408.000   | 476.000   | 17204.000 | 17272.000 | 17340.000 
#Fan4             | na         | RPM        | na    | 340.000   | 408.000   | 476.000   | 17204.000 | 17272.000 | 17340.000 
#PS Status        | 0.000      | unspecified | ok    | na        | na        | na        | na        | na        | na        

set exitStatus ok
set exitMessage ""
set exitSeparator ""
set foundData 0

set ipmiCmd $ipmiCmdPrefix
append ipmiCmd " sel info"
set chanId [open |$ipmiCmd r]
set selCount 0
while {![eof $chanId]} {
    gets $chanId line
    if {[regexp {^Entries .*: ([0-9]+)} $line null match1]} {
	set selCount $match1
    }
}
close $chanId

set ipmiCmd $ipmiCmdPrefix
append ipmiCmd " sensor list"
#puts "Executing $ipmiCmd"
set chanId [open |$ipmiCmd r]
while {![eof $chanId]} {
    gets $chanId line
#puts stderr "Read line $line"
    if {$line != ""} {
	set foundData 1
	foreach {name value units status lnr lc lw uw uc unr} [split $line |] {}
	set name [string trim $name]
	set status [string trim $status]
	set units [string trim $units]

        if {[lsearch $option(ignore) $name] == -1} {
            # Don't try to validate discrete sensors because we don't know how
            # to interpret the values.
            if {$status != "ok" && $status != "na" && $units != "discrete"} {
                # Until we know what values to expect for the warning
                # and critical states, we will assume that all non-ok states
                # are critical
                set exitStatus critical
                append exitMessage "${exitMessage}${exitSeparator} [string trim $name]: [string trim $value] [string trim $units]"
                set exitSeparator ","
            }
        }
    }
}

# TODO:  Include the size of the event log, and trigger a warning/error
# if the -e option was given.

if {!$foundData} {
    nagios::exit_unknown "No data returned from ipmi query"
}

if {$exitStatus != "ok"} {
    nagios::exit_critical $exitMessage
}

if {$option(e)} {
    if {$selCount > $option(c)} {
	nagios::exit_critical "Found $selCount ipmi events"
    } elseif {$selCount > $option(w) } {
	nagios::exit_warning "Found $selCount ipmi events"
    }
}

nagios::exit_ok "All sensors within allowed thresholds"
