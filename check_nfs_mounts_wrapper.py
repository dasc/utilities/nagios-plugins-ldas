#!/usr/bin/python3

import sys, subprocess, os

f=open("/etc/fstab","r")
lines=map(lambda x: x.split(), f.readlines())
f.close()

#print "\n".join(map(repr,lines))

nfsmounts=map(lambda y: y[1], filter(lambda x: len(x)>3 and x[2].find("nfs")==0 and x[0][0] != '#', lines))

com=["/usr/lib64/nagios/plugins/check_nfs_mounts.sh"]
com.extend(nfsmounts)

a=subprocess.run(com, stdout=subprocess.PIPE, encoding='utf-8')
print(a.stdout)
sys.exit(a.returncode)
