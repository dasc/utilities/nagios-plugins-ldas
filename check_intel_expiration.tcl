#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName INTEL_LICENSE

set options $nagios::options
lappend options \
    [list file.arg {/ldcg/intel/licenses/non-commercial.lic} "Path to license file"] \
    [list w.arg {2 weeks} "Amount of time left before issuing a warning alert"] \
    [list c.arg {0 seconds} "Amount of time left before issuing a critical alert"]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

set fileId [open $option(file) r]

set date(expiration) {}
set date(now) [clock seconds]

while {![eof $fileId]} {
    gets $fileId line
    if [regexp {^INCREMENT [^ ]* INTEL [^ ]* ([^ ]*) uncounted} $line null expirationDate] {
        set date(expiration) $expirationDate
    }
}

if {$date(expiration) == ""} {
    nagios::exit_unknown "No expiration date found in $option(file)"
}

if {[clock scan $date(expiration)] < $date(now)} {
    set verb expired
} else {
    set verb expires
}

if {[clock scan "$date(expiration) - $option(w)"] > $date(now)} {
    nagios::exit_ok "License $verb on $date(expiration)"
} elseif {[clock scan "$date(expiration) - $option(c)"] > $date(now)} {
    nagios::exit_warn "License $verb on $date(expiration)"
} else {
    nagios::exit_critical "License $verb on $date(expiration)"
}
