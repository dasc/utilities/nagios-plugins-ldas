#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName SWITCH_ERRORS

set options $nagios::options
lappend options \
    [list H.arg {} "Switch address"] \
    [list C.arg {public} "SNMP community.  Defaults to 'public'"] \
    [list state.arg {} "State file containing previous value"]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

if {$option(state) == ""} {
    nagios::exit_unknown "state file must be given with the -state flag"
}
if {$option(H) == ""} {
    nagios::exit_unknown "Switch address must be given with the -H flag""
}

if {[file exists $option(state)] && ![file readable $option(state)]} {
    nagios::exit_unknown "Can't open $option(state) for reading"
}

proc readStateFile {statefile} {
    # The format of the state file is "port value"
    set chanId [open $statefile r]
    while {![eof $chanId]} {
        gets $chanId line
        if {[regexp {^([^\s]+)\s*(.*)$} $line null key value]} {
            set state($key) $value
        }
    }
    close $chanId

    return [array get state]
}

proc writeStateFile {statefile stateArr} {
    array set state $stateArr
    set chanId [open $statefile w]
    foreach key [lsort [array names state]] {
        puts $chanId "$key $state($key)"
    }
    close $chanId
}

if {[catch {array set lastRunErrors [readStateFile $option(state)]} errMsg]} {
    if {[file exists $option(state)]} {
	nagios::exit_critical "Error reading from state file: $errMsg"
    }
}

proc readPortErrors {community host direction} {
    set cmd [list snmpwalk -v2c -c $community $host IF-MIB::if${direction}Errors]

    set chanId [open "|$cmd" r]
    while {![eof $chanId]} {
	gets $chanId line
# IF-MIB::ifOutErrors.51 = Counter32: 0
	if {[regexp "^IF-MIB::if${direction}Errors.(\\d+) = Counter\\d+: (\\d+)" $line null port counter]} {
	    incr errors($port) $counter
	}
    }
    if {[catch {close $chanId} errMsg]} {
	nagios::exit_unknown "Error looking up switch errors: $errMsg"
    }

    array get errors
}

array set errors [readPortErrors $option(C) $option(H) In]
array set outErrors [readPortErrors $option(C) $option(H) Out]
foreach port [array names outErrors] {
    if {[info exists errors($port)]} {
	incr errors($port) $outErrors($port)
    } else {
	set errors($port) $outErrors($port)
    }
}

writeStateFile $option(state) [array get errors]

set msg ""
foreach port [lsort [array names errors]] {
    if [info exists lastRunErrors($port)] {
	if {$errors($port) - $lastRunErrors($port)} {
	    append msg "Port $port: [expr $errors($port) - $lastRunErrors($port)]"
	}
    }
}

if {$msg != ""} {
    nagios::exit_critical $msg
} else {
    nagios::exit_ok "No new port errors found"
}
