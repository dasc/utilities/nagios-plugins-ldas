#!/usr/bin/tclsh

package require cmdline
package require nagios 1.1

# This must be defined before the command line options are processed
# because it can be overridden by the -service option.
set nagios::serviceName EXPORTS

set options $nagios::options
lappend options \
    [list H.arg {} "Name of NFS server to check"] \
    [list c.arg {} "Number of exported filesystems to expect"]

# Process the standard Nagios command line options.  We pass the
# plugin-specific options for useful error reporting.
nagios::process_options $options argv

if {[catch {
    foreach {var val} [::cmdline::getoptions argv $options] {
        set option($var) $val
    }
} msg]} {
    puts stderr $msg
    exit 1
}

if {$option(H) == ""} {
    nagios::exit_unknown "Missing argument -H"
}

set cmd "showmount -e $option(H)"

set exports {}

set chanId [open "|$cmd" r]
while {![eof $chanId]} {
    gets $chanId line
    if {[llength $line] == 2} {
        lappend exports [lindex $line 1]
    }
}

if {[catch {close $chanId} msg]} {
    nagios::exit_critical $msg
}

if {$option(c) != "" && ![nagios::compareToRange [llength $exports] $option(c)]} {
    nagios::exit_critical "[llength $exports] mounts found.  Expected $option(c)"
}

nagios::exit_ok "[llength $exports] mounts found."
